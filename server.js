let express = require('express');
let socket = require('socket.io');
let bodyParser = require('body-parser');


let app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let server = app.listen(3000 , ()=>{
	console.log('Listening on Port 3000..');
});


app.use(express.static('public'));

let lastSocket = null;

app.put('/geo',(req,res,next)=>{

	console.log('Request Received'+req.body.latitude+'  kkk');

	let lat = req.body.latitude;
	let long = req.body.longitude;
	let speed= req.body.value;

	lastSocket.emit('geo', {lat:lat, long:long,speed:speed});

	res.send('Message Received');

});


let io = socket(server);

io.on('connection', (socket)=>{

	lastSocket = socket;

	console.log('Socket Connected..');

});
